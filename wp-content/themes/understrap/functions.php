<?php
/**
 * Understrap functions and definitions
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$understrap_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
);

foreach ( $understrap_includes as $file ) {
	$filepath = locate_template( '/inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}
function admin_bar(){

	if(is_user_logged_in()){
	  add_filter( 'show_admin_bar', '__return_true' , 1000 );
	}
  }
  add_action('init', 'admin_bar' );
/*-------------------------------------------------------------------------------------------------------------------------*/




if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}

/* filters------------------------------------------------ */
function my_filters(){
    $args = array(
        'orderby' => 'date',
        'order' => $_POST['date']
    );
  
        if( isset( $_POST['categoryfilter'] ) )
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'category',
                'field' => 'id',
                'terms' => $_POST['categoryfilter']
            )
        );
   
    $query = new WP_Query( $args );
  
	if( $query->have_posts() ) :
		$post_counter = 0;
		while( $query->have_posts() ): $query->the_post();
			$backgroundPost = get_field('featured_color', $query->ID);
			$postTitle = $query->post->post_title;
			$backgroundImagePost = get_the_post_thumbnail_url(get_the_ID($query->ID),'full');  
			$homepageSize = get_field('homepage_size', $query->ID);
			$secundaryTitle = get_field('secundary_title',$query->ID);
			$enable_gradient = get_field('enable_gradient',$query->ID);
			$featured_gradient = get_field('featured_gradient',$query->ID);
			$is_featured_image_the_background = get_field('is_featured_image_the_background',$query->ID);
			$test = $is_featured_image_the_background;
			$permalink = get_permalink( $query->ID );
			$username = get_the_author_meta('user_nicename');
			$user_url_archive =  get_author_posts_url( $query->ID , $username ) ;
			$image_avatar =  get_avatar( get_the_author_meta('user_email'), $size = '50');
			$post_date = get_the_date('Y-m-d');
			$author_post_name = get_the_author($query->ID); 
				if ($homepageSize == "small") {
					if ( $enable_gradient ){	
						$backgroundPost = 'style=" background-image: linear-gradient(to bottom, ' . $backgroundPost. ', ' . $featured_gradient. '  );"'; 
					}else{
						$backgroundPost = 'style="background-color: '. $backgroundPost .'"';	
					}
					if ( $is_featured_image_the_background ){	
						
						$backgroundPost = 'style="background: url('. $backgroundImagePost .');    
						background-repeat: no-repeat; 
						background-size: cover;
						background-position: 50% 50%; "'.$backgroundImagePost; 
					}
					echo "<div class='small grid-item'  ". $backgroundPost . ">
						
						 <div class='small-grid-content'>
						 	<a href='". $permalink . "'> 
								<h1>". $postTitle ." </h1>
								<h3>". $secundaryTitle ." </h3>
							</a>
						</div>
						<div class='avatar'><a href='". $user_url_archive."'>".$image_avatar."</a> 
							<div class='post-author-data'><a href='". $user_url_archive."'> <h3>".$author_post_name."</h3><p>".$post_date."</p></a></div>
						</div>
												
					</div>";   

				} elseif ($homepageSize == "small-tall") {
					if ($enable_gradient){
						$backgroundPost =  'style=" background-image: linear-gradient(to bottom, ' . $backgroundPost. ', ' . $featured_gradient. '  );"'; 
					}
					else { 
						$backgroundPost = 'style= "background: '. $backgroundPost.' ;"  '; 
					}
					echo "<div class='small-tall grid-item'>
							<div style='background: url(".$backgroundImagePost .")'; class='top-section'>
							</div>
							<div  ". $backgroundPost . " class='bottom section'>
									<div class='content-tall'>  
										<a href='".$permalink." '>
											<h1>". $postTitle ." </h1>  
											<p>". $secundaryTitle ." </p>
										</a>
									</div>
								<div class='avatar'><a href='". $user_url_archive."'>".$image_avatar."</a> 
									<div class='post-author-data'><a href='". $user_url_archive."'> <h3>".$author_post_name."</h3><p>".$post_date."</p></a></div>
								</div>	
							</div>
					</div>";

				} elseif ($homepageSize == "medium") {
					
					if ($enable_gradient){
						$backgroundPost =  'style=" background-image: linear-gradient(to bottom, ' . $backgroundPost. ', ' . $featured_gradient. '  );"'; 
					}
					else { 
						$backgroundPost = 'style= "background: '. $backgroundPost.' ;"  '; 
					}
					echo "<div class='medium grid-item'>

							<div  ". $backgroundPost . " class='right section'>
									<div class='content-medium'>  
										<a href='".$permalink." '>
											<h1>". $postTitle ." </h1>  
											<p>". $secundaryTitle ." </p>
										</a>
									</div>
									<div class='avatar'><a href='". $user_url_archive."'>".$image_avatar."</a> 
										<div class='post-author-data'><a href='". $user_url_archive."'> <h3>".$author_post_name."</h3><p>".$post_date."</p></a></div>
									</div>	
							</div>
							<div style='background: url(".$backgroundImagePost .")'; class='left-section'>
							</div>
					</div>";
				}
				elseif ($homepageSize == "large") {
					if ($enable_gradient){
						$backgroundPost =  'style=" background-image: linear-gradient(to bottom, ' . $backgroundPost. ', ' . $featured_gradient. '  );"'; 
					}
					else { 
						$backgroundPost = 'style= "background: '. $backgroundPost.' ;"  '; 
					}
					echo "<div class='large grid-item'>
							<div style='background: url(".$backgroundImagePost .")'; class='left-section'>
							</div>
							<div  ". $backgroundPost . "class='right-large-container'>
									<div class='large-content'>  
										<a href='".$permalink." '>
											<h1>". $postTitle ." </h1>  
											<p>". $secundaryTitle ." </p>
										</a>
									</div>
									<div class='avatar'><a href='". $user_url_archive."'>".$image_avatar."</a> 
										<div class='post-author-data'><a href='". $user_url_archive."'> <h3>".$author_post_name."</h3><p>".$post_date."</p></a></div>
									</div>	
							</div>
					</div>";
				}
			$post_counter++; 
		endwhile;
			echo "
			<script>
			jQuery(document).ready(
						function() {
							jQuery('span.total-post').text('".$post_counter."');
						}
					);
			</script>	
			";
        wp_reset_postdata();
    else :
        echo 'No posts found';
    endif;
    die();
}  
add_action('wp_ajax_customfilter', 'my_filters');
add_action('wp_ajax_nopriv_customfilter', 'my_filters');


function homepage_search(){
	global $wpdb;
    $args = array(
        'orderby' => 'date',
        'order' => $_POST['date'],
        's' => $_POST['whatever'],
        'post_type' => 'post'
	);
	$query = new WP_Query( $args );
	if( $query->have_posts() ) :
		$post_counter = 0;
		while( $query->have_posts() ): $query->the_post();
			$backgroundPost = get_field('featured_color', $query->ID);
			$postTitle = $query->post->post_title;
			$backgroundImagePost = get_the_post_thumbnail_url(get_the_ID($query->ID),'full');  
			$homepageSize = get_field('homepage_size', $query->ID);
			$secundaryTitle = get_field('secundary_title',$query->ID);
			$enable_gradient = get_field('enable_gradient',$query->ID);
			$featured_gradient = get_field('featured_gradient',$query->ID);
			$is_featured_image_the_background = get_field('is_featured_image_the_background',$query->ID);
			$test = $is_featured_image_the_background;
			$permalink = get_permalink( $query->ID );
			$user_url_archive =  get_home_url() ."/author/". get_the_author_link($query->ID);
			$image_avatar =  get_avatar( get_the_author_meta('user_email'), $size = '50');
			$post_date = get_the_date('Y-m-d');
			$author_post_name = get_the_author($query->ID); 
				
					if ( $enable_gradient ){	
						$backgroundPost = 'style=" background-image: linear-gradient(to bottom, ' . $backgroundPost. ', ' . $featured_gradient. '  );"'; 
					}else{
						$backgroundPost = 'style="background-color: '. $backgroundPost .'"';	
					}
					if ( $is_featured_image_the_background ){	
						
						$backgroundPost = 'style="background: url('. $backgroundImagePost .');    
						background-repeat: no-repeat; 
						background-size: cover;
						background-position: 50% 50%; "'.$backgroundImagePost; 
					}
					echo "<div class='small grid-item'  ". $backgroundPost . ">
						
						 <div class='small-grid-content'>
						 	<a href='". $permalink . "'> 
								<h1>". $postTitle ." </h1>
								<h3>". $secundaryTitle ." </h3>
							</a>
						</div>
						<div class='avatar'><a href='". $user_url_archive."'>".$image_avatar."</a> 
							<div class='post-author-data'><a href='". $user_url_archive."'> <h3>".$author_post_name."</h3><p>".$post_date."</p></a></div>
						</div>
												
					</div>";   
					$post_counter++;
		endwhile;
			echo "
			<script> 
			
							jQuery('span.total-post').text('".$post_counter."');
						
			</script>	
			";
        wp_reset_postdata();
    else :
        echo 'No posts found';
    endif;
    die();
}


add_action( 'wp_ajax_search', 'homepage_search' );
add_action( 'wp_ajax_nopriv_search', 'homepage_search' );
/* filters------------------------------------------------ */


