<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


get_header();
wp_reset_query();
$homeURL = get_site_url();
$container   = get_theme_mod( 'understrap_container_type' );
$postID = get_the_ID();
$postContent = get_post_field('post_content', $postID);
$postAuthor = get_post_field('post_author', $postID); 
$postTitle = get_post_field('post_title', $postID);
$postSlug = get_post_field( 'post_name', get_post() );
$postTags = get_the_tags( $postID );

$username = get_the_author_meta('user_nicename');
$authorURL =  get_author_posts_url( $postID , $username ) ;
$image_avatar =  get_avatar(get_the_author_meta('user_email'), $size = '');
$authorName = get_the_author_meta('last_name');
$post_date = get_the_date('Y-m-d');

/*ACF variable declaration------------------------------------------------------------------*/ 
$backgroundPost = get_field('featured_color', $postID);  
$backgroundImagePost = get_the_post_thumbnail_url(get_the_ID($postID),'full');  
$homepageSize = get_field('homepage_size', $postID);
$secundaryTitle = get_field('secundary_title',$postID);
$enable_gradient = get_field('enable_gradient',$postID);
$featured_gradient = get_field('featured_gradient',$postID);
$is_featured_image_the_background = get_field('is_featured_image_the_background',$postID);
$test = $is_featured_image_the_background;
$permalink = get_permalink( $postID );
$user_url_archive =  get_home_url() ."/author/". get_the_author_link($postID);

if ($enable_gradient){
	$backgroundPost = 'style=" background-image: linear-gradient(to bottom, ' . $backgroundPost. ', ' . $featured_gradient. '  );"'; 
}
else{
	$backgroundPost = "style= 'background: ". $backgroundPost.";' ";
}
if ( $is_featured_image_the_background ){	
						
	$backgroundPost = 'style="background: url('. $backgroundImagePost .');    
	background-repeat: no-repeat; 
	background-size: cover;
	background-position: 50% 50%; "'.$backgroundImagePost; 	
}
?>

<div class="container" >    
		<div <?php echo $backgroundPost?> class="featured-post-image row">
			<div class="col-lg-3">

			</div>
			<div class="col-lg-9">
			<h1 class="single-post-title"> <?php echo $postTitle  ?></h1>
				<div class="featured-post-image-content">
						<h2> <?php echo $secundaryTitle  ?></h2>
						<div class="author-meta-single-post">
						<a href="<?php echo $authorURL; ?> ">	
							<?php echo $image_avatar;?>
							<span><?php echo $authorName;?></span>
						</a>		
						</div>
				</div>
			</div>		
		</div>
		<div class="row">
			<div class="col-lg-3">
			<ul class="list-group post-list">
				

			</ul>
			</div>
			<div class="col-lg-9">
				<div class="row post-tags-and-social">
					<div class="single-tag-list col-lg-6">
						<?php
							$posttags = get_the_tags();
							if ($posttags) {
								echo "<span>Tags: </span>";
								$totalTags = count($posttags);
								$counter = 0;								
								foreach($posttags as $tag) {
									$counter++;
									echo "<a href='". $homeURL."/tag/" .  $tag->slug." '> "   .$tag->name."</a>";	
									if (!( $counter == $totalTags )){
										echo "<span>,</span>";
									}
								}
								 
							}
						?>
					</div>	
					<div class="social-media-post col-lg-6">
						<?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
					</div>			
				</div>


				<hr>
				<div class="post-content-container">
					<?php echo get_post_field('post_content', $post_id);?>
				</div>
					<div id="disqus_thread"></div> <!-- /.disqus -->				
			</div>
		</div>
	</div>
</div>


<script>
jQuery( document ).ready(function() {
	const content = jQuery( ".post-content-container" ).html();
	fetchTitles = content.match(/<h2(.*?)>(.*?)<\/h2>/g);
	var arrayLength = fetchTitles.length;
	for (var i = 0; i < arrayLength; i++) {
		var p = fetchTitles[i];
		var firstTrim = /<h2(.*?)>|<h3(.*?)>/gi;
		var secondTrim = /<\/h2>|<\/h3>/gi;
		p = p.replace(firstTrim, '');
		p = p.replace(secondTrim, '');
		jQuery('.list-group.post-list').append('<li class="list-group-item scroll-to-'+i+'"> <a href="#'+(i+1)+'">  '+p+'  </a></li>');   /*here*/
	}
	i = 0;
	jQuery("h2").each(function(i) {
		jQuery(this).addClass("post-title-" + (i));
		jQuery(this).addClass('single-title');
		jQuery(this).attr('id',i);
	});
	
	jQuery('li.list-group-item.scroll-to-0:first-child').addClass('active');	
	jQuery('ul.list-group.post-list .list-group-item').click(function(){
		jQuery('ul.list-group.post-list .list-group-item').removeClass('active');
		jQuery(this).addClass('active');
	});

	setTimeout(function() {
		console.log('ready');

		(function() {
			'use strict';

			var section = document.querySelectorAll(".single-title");
			var sections = {};
			var i = 0;
			var titleList = [];
			Array.prototype.forEach.call(section, function(e) {
				sections[e.id] = e.offsetTop;
				var x = jQuery('.post-content-container').offset().top;
				console.log(x);
				titleList.push((e.offsetTop) + x );
				console.log(titleList);
			});
			window.onscroll = function() {
				var scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;

				for (i in titleList) {

					if (titleList[i] <= scrollPosition) {
						jQuery("ul.list-group.post-list li").removeClass('active');
						jQuery('.scroll-to-'+i).addClass('active');
						jQuery('.list-group-item').click(function(){
						});


					 }
				}
			};
			jQuery('li.list-group-item').click(function(){
				jQuery('li.list-group-item').removeClass('active');
				jQuery(this).addClass('active');	

				
			});
	
		


				var $sticky = jQuery('ul.list-group.post-list');
				var $stickyrStopper = jQuery('div#wrapper-footer');
				if (!!$sticky.offset()) { // make sure ".sticky" element exists

					var generalSidebarHeight = $sticky.innerHeight();
					var stickyTop = $sticky.offset().top;
					var stickOffset = 0;
					var stickyStopperPosition = $stickyrStopper.offset().top;
					var stopPoint = stickyStopperPosition - generalSidebarHeight - stickOffset;
					var diff = stopPoint + stickOffset;

					jQuery(window).scroll(function(){ // scroll event
					var windowTop = jQuery(window).scrollTop(); // returns number

					if (stopPoint < windowTop) {
						$sticky.css({ position: 'absolute', top: diff });
						$sticky.removeClass(sticky);
						console.log('removing class');
					} else if (stickyTop < windowTop+stickOffset) {
						$sticky.css({ position: 'fixed', top: stickOffset });
						$sticky.addClass('sticky');

						console.log('adding class');
					} else {
						$sticky.css({position: 'absolute', top: 'initial'});

						$sticky.removeClass('sticky');
						console.log('removing class');
					}
					});
				
				var singleWidth = jQuery('ul.list-group.post-list').parent().width(); 
				jQuery('ul.list-group.post-list').css('width', (singleWidth + 'px')  );
				jQuery(window).resize(function(){
					singleWidth = jQuery('ul.list-group.post-list').parent().width(); 
					jQuery('ul.list-group.post-list').css('width', (singleWidth + 'px')  );	
				});

				}
		})();
    }, 1000);
});





</script>





<?php get_footer(); ?>
