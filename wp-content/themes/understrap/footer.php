
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'sidebar-templates/sidebar', 'footerfull' ); ?>


<div class="wrapper" id="wrapper-footer">


	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">
			<div class="col-md-12">

				<footer class="site-footer" id="colophon">
					<div class="container">
						<div class="row">
							<div class="col-md-3"><a href="<?php  echo get_home_url();?>"><img class="img-responsive" src="<?php the_field('footer_logo', 'option');?>" alt=""></a></div>
							<div class="col-md-3">
								
								<H3>Site Map</H3>
								<ul>
									<li><a href="">Blog</a></li>
									<li><a href="">Join Our Network</a> </li>
									<li><a href="">Case Studies</a>	</li>
									<li><a href="">Co-Working Space</a> </li>
								</ul>
							</div>
							<div class="col-md-3">
							<H3>Contact Us</H3>
								<ul class="contact-us-footer">
									<li><a href=""> <span class="logo-location"></span>	 715 J Street Suite 200,
													San Diego, CA 92101</a></li>
									<li><a href=""> <span class="logo-phone"></span>858-935-8800</a></li>
								</ul>
								
							</div>
							<div class="col-md-3">
							<H3>Social</H3>
								<ul class="social-media-footer-icons">
                                    <li><a href="https://www.facebook.com/designstudionetwork/?fref=ts"></a><span class="footer-icons facebook"></span> </li>
                                    <li><a href="https://twitter.com/designstudionet"></a><span class="footer-icons twitter"></span> </li>
                                    <li><a href="https://www.linkedin.com/company/designstudio-network"></a><span class="footer-icons linkedin"></span>	</li>
                                    <li><a href=""></a><span class="footer-icons instagram"></span> </li>
								</ul>
							</div>
						<p class="footer-rights">© <?php echo date("Y"); ?> DESIGNSTUDIO. ALL RIGHTS RESERVED</p>
						</div>
					</div>

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>


<script>
	
jQuery( document ).ready(function() {
	jQuery(document).on('mouseover', '.avatar', function(){
		jQuery(this).addClass('hover');
		jQuery(this).prev().addClass('hidden');
	});

	jQuery(document).on('mouseout', '.avatar', function(){
		jQuery(this).removeClass('hover');
		jQuery(this).prev().removeClass('hidden');
	});
});

</script>
</body>

</html>

