<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<!-- <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
 -->
	
		<?php if ( 'post' == get_post_type() ) : ?>
		<?php 
			$backgroundPost = get_field('featured_color', $query->ID);
			$postTitle = get_the_title(  $query->ID  );

			$backgroundImagePost = get_the_post_thumbnail_url(get_the_ID($query->ID),'full');  
			$homepageSize = get_field('homepage_size', $query->ID);
			$secundaryTitle = get_field('secundary_title',$query->ID);
			$enable_gradient = get_field('enable_gradient',$query->ID);
			$featured_gradient = get_field('featured_gradient',$query->ID);
			$is_featured_image_the_background = get_field('is_featured_image_the_background',$query->ID);
			$test = $is_featured_image_the_background;
			$permalink = get_permalink( $query->ID );
			$username = get_the_author_meta('user_nicename');
			$user_url_archive =  get_author_posts_url( $query->ID , $username ) ;
			$image_avatar =  get_avatar( get_the_author_meta('user_email'), $size = '50');
			$post_date = get_the_date('Y-m-d');
			$author_post_name = get_the_author($query->ID); 		

				if ( $enable_gradient ){	
					$backgroundPost = 'style=" background-image: linear-gradient(to bottom, ' . $backgroundPost. ', ' . $featured_gradient. '  );"'; 
				}else{
					$backgroundPost = 'style="background-color: '. $backgroundPost .'"';	
				}
				if ( $is_featured_image_the_background ){	
					
					$backgroundPost = 'style="background: url('. $backgroundImagePost .');    
					background-repeat: no-repeat; 
					background-size: cover;
					background-position: 50% 50%; "'.$backgroundImagePost; 
				}
				echo "<div class='small grid-item'  ". $backgroundPost . ">
					
					 <div class='small-grid-content'>
						 <a href='". $permalink . "'> 
							<h1>". $postTitle ." </h1>
							<h3>". $secundaryTitle ." </h3>
						</a>
					</div>
					<div class='avatar'><a href='". $user_url_archive."'>".$image_avatar."</a> 
						<div class='post-author-data'><a href='". $user_url_archive."'> <h3>".$author_post_name."</h3><p>".$post_date."</p></a></div>
					</div>
											
				</div>";  



		?>
		<?php endif; ?>

	

	



<!-- </article> -->
