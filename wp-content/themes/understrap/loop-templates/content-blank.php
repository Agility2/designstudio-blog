<?php
get_header();
/**
 * Blank content partial template.
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<div class="container"> 
  <div class="row">
  <?php 


$posts = get_field('hero-slider');   
if( $posts ): ?>
	<ul class="hero-slider" >
    <?php foreach( $posts as $p ): // variable must NOT be called $post (IMPORTANT) ?>
    <?php   
     $sliderColor = get_field('featured_color', $p->ID);
     $sliderFeatured_gradient = get_field('featured_gradient',$p->ID);
     $sliderIsFeaturedImageBackground = get_the_post_thumbnail_url($p->ID,'full');
     $sliderBackground = "";
     $user_url =  get_home_url() ."/author/". get_the_author_link($p->ID);

        if ( $sliderFeatured_gradient ){	
            $sliderBackground = ' background-image: linear-gradient(to bottom, ' . $sliderColor. ', ' . $sliderFeatured_gradient. '  );"'; 
        }else{
            $sliderBackground = "background: ". $sliderColor;	
        }
    ?>
	    <li class="hero-slider-element">
	    	<span> <?php   ?></span>  
        <!--<div  class="hero-image" style='background: url( <?php echo get_the_post_thumbnail_url( $p->ID, 'thumbnail' ); ?> )' >-->
        <div  class="hero-image" style='background: url( <?php echo $sliderIsFeaturedImageBackground ?> )' >
        </div>

        <div  class="hero-color" style='  <?php echo $sliderBackground ?> ' >

            <div class="hero-title"><a href="<?php echo get_permalink( $p->ID ); ?>"><h1><?php echo get_the_title( $p->ID ); ?></h1></a></div>         
            <div class="hero-avatar"><a href="<?php echo $user_url;?> "> <?php echo get_avatar( get_the_author_meta('user_email'), $size = '50'); ?></a></div>
        </div>
	    </li>
	<?php endforeach; ?>
	</ul>   
<?php  endif; wp_reset_query(); ?>


<div class= "filters">  
    <div class="category-filter-container">
        <p class="category-selector">Showing: <span class="total-post"></span><span class="ds-bolder"> posts</span> in    </p> 
        <script>
        function eventFire(el, etype){
        if (el.fireEvent) {
            el.fireEvent('on' + etype);
        } else {
            var evObj = document.createEvent('Events');
            evObj.initEvent(etype, true, false);
            el.dispatchEvent(evObj);
        }
        }
        </script>
        <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="filter" >
            <?php
                if( $terms = get_terms( 'category', 'orderby=name' ) ) :
                    echo '<select  name="categoryfilter"><option>Everything</option>';
                    foreach ( $terms as $term ) :
                        if ( $term->term_id == 1 ) {
                            
                            }else {
                                echo '<option value="' . $term->term_id . '">' . $term->name . '</option>'; 
                            }
                    endforeach;
                    echo '</select>';
                endif;
            ?>
            <button id="filter-button">Apply filters</button>
            <input id="filter-button" type="hidden" name="action" value="customfilter">
        </form>
    </div>
    



    <form class="form-wrapper"  role="search" method="get" action="<?php echo get_home_url();?> ">
            <label class="screen-reader-text" for="s">Search for:</label>
            <div class="goSearch">
                <input class="popup-search-bar search" type="search" placeholder="Search Posts" value="" name="s" title="Search for:">
                <input  class="search-button" type="Submit" value=" "  onclick="return false;">
    <!--            <a class="goSearch" href="javascript:;">Go</a>-->
            </div>
            <input type="hidden" name="post_type" value="product">
    </form>
</div>
<div id="response"></div>

  <?php 
    the_content();  
  ?>


  
  </div>
</div>



<?php
get_footer();

?>

<script>
jQuery(document).ready(function(){
  jQuery('.hero-slider').slick({
    arrows: true
  });
  setTimeout(function() {
      jQuery( "#filter" ).trigger( "change" );
  }, 10);
  
});
jQuery(function($){
    $('#filter').change(function(){

        var filter = $('#filter');
        $.ajax({
            url:filter.attr('action'),
            data:filter.serialize(), // form data
            type:filter.attr('method'), // POST
            beforeSend:function(xhr){
                filter.find('button').text('Applying Filters...');          },
            success:function(data){
                filter.find('button').text('Apply filters');                
                $("#response").fadeOut(200, function() {
                    $('#response').html(data);
                    $("#response").fadeIn();
                });
            }
        }); 
        return false;
    });
});


jQuery(document).ready(function($) {
    $('input.search-button').click(function(){
        var searchPar = $('input.popup-search-bar.search').val();
        var data = {
            'action': 'search',
            'whatever': searchPar      // We pass php values differently!
        };
        // We can also pass the url value separately from ajaxurl for front end AJAX implementations
        jQuery.post('wp-admin/admin-ajax.php', data, function(response) {
           
            $("#response").fadeOut(200, function() {
                $('#response').html(response); 
                $("#response").fadeIn();
            });
           
        });
    });

});

</script>


<?php
