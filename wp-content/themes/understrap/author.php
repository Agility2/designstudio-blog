<?php
/**
 * The template for displaying the author pages.
 *
 * Learn more: https://codex.wordpress.org/Author_Templates
 *
 * @package understrap
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$author_id = get_the_author_meta('ID');
$author_role = get_field('role_in_the_company', 'user_'. $author_id);
$company_name = get_field('company','user_'.$author_id);
$author_location = get_field('location','user_'.$author_id);
$author_about = get_field('about','user_'.$author_id);
$author_website = get_field('companys_website','user_'.$author_id);
$query = new WP_Query( array( 'author' => $author_id));
?>

<div class="container">
	<div class="row">
		<div class="col-lg-4 author-container">
			<div class="title-spotlight">Author Spotlight</div>
			<div class="author-spotlight">
				<div class="row no-padding">
					<div class="col-lg-4 author-avatar-image">
						<?php echo get_avatar( get_the_author_meta('user_email'), $size = '');?>
					</div>
					<div class="col-lg-8">
						<div class="author-meta">
							<?php  echo "<div class='author-name-title'>". get_author_name() ."</div>";?>
							<?php echo "<div class='author-post-count'>". count_user_posts($author_id)." Posts </div>";?>						
						</div>

					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<?php 
						 echo $author_role ;
						?>
					</div>
					<div class="col-lg-12">
					<?php 
						 echo "<a href='". $author_website." '>@". $company_name ."</a>";
					?>
					</div>
					<div class="col-lg-12">
					<?php 
						 echo $author_location ;
					?>
					</div>
					<hr>
					<div class="col-lg-12">
					<div class="about-author-title">
					    <?php  echo 'About '.get_the_author_meta( 'nickname', $author_id );?>  
					</div>
					<?php 
						 echo $author_about;
					?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-8">
			<div class="author-archive-container">
			<?php 
					if ( $query->have_posts() ) {
						while ( $query->have_posts() ) {
							$query->the_post();
							$enable_gradient = get_field('enable_gradient',$query->ID);
							$backgroundPost = get_field('featured_color', $query->ID);
							$featured_gradient = get_field('featured_gradient',$query->ID);
							$is_featured_image_the_background  = get_field('is_featured_image_the_background',$query->ID);
							$backgroundImagePost = get_the_post_thumbnail_url(get_the_ID($query->ID),'full');
							$permalink = get_permalink( $query->ID );
							$postTitle = $query->post->post_title;
							$secundaryTitle = get_field('secundary_title',$query->ID);
							

							if ( $enable_gradient ){	
								$backgroundPost = 'style=" background-image: linear-gradient(to bottom, ' . $backgroundPost. ', ' . $featured_gradient. '  );"'; 
							}else{
								$backgroundPost = 'style="background-color: '. $backgroundPost .'"';	
							}
							if ( $is_featured_image_the_background ){	
								
								$backgroundPost = 'style="background: url('. $backgroundImagePost .');    
								background-repeat: no-repeat; 
								background-size: cover;
								background-position: 50% 50%; "'.$backgroundImagePost; 
							}
							echo "<div class='small grid-item'  ". $backgroundPost . ">
								
								<div class='small-grid-content'>
									<a href='". $permalink . "'> 
										<h1>". $postTitle ." </h1>
										<h3>". $secundaryTitle ." </h3>
									</a>
								</div>

														
							</div>";   
						}
						// Restore original Post Data
						wp_reset_postdata();
					}
				?>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>
